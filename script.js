function startGame() {
    myGamePiece = new component(30, 30, "red", (window.innerWidth - 25) / 2, (window.innerHeight - 25) / 2, 'player');
    if (localStorage.getItem("personalBest") === null) {
        localStorage.setItem("personalBest", 0);
    }
    myGameArea.start();
}

var myGameArea = {
    // definiranje game area-e, kod modificiran iz prezentacije
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.id = "myGameCanvas";
        this.canvas.width = window.innerWidth - 25;
        this.canvas.height = window.innerHeight - 25;
        this.canvas.style.border = "2px solid black";
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.frameNo = 0;
        this.personalBest = localStorage.getItem("personalBest");
        this.startTime = new Date();
        this.elapsedTime = 0;
        this.asteroids = [];
        //maksimalna i minimalna visina/sirina inicijalnog polozaja asteroida (asteroidi se stvaraju izvan canvasa)
        this.maxAsteroidHeight = window.innerHeight + 1000;
        this.maxAsteroidWidth = window.innerWidth + 1000;
        this.min = -1000;
        var grd = this.canvas.getContext("2d").createLinearGradient(0, 0, 170, 0);
        grd.addColorStop(0, "gray");
        grd.addColorStop(1, "white");

        for (var i = 0; i < 50; i++) {
            // generiranje nasumicne visine i sirine polazaja asteroida sve dok se ne nalazi izvan canvasa
            let randomWidth, randomHeight;
            do {
                randomWidth = Math.floor(Math.random() * (window.innerWidth + 500 - (-500) + 1)) - 500;
            } while (randomWidth >= 0 && randomWidth <= window.innerWidth);

            do {
                randomHeight = Math.floor(Math.random() * (window.innerHeight + 500 - (-500) + 1)) - 500;
            } while (randomHeight >= 0 && randomHeight <= window.innerHeight);

            var asteroid = new component(30, 30, grd, randomWidth, randomHeight, 'asteroid');
            this.asteroids.push(asteroid);
        }
        this.interval = setInterval(updateGameArea, 20);
        this.createNewAsteroidInterval = setInterval(addNewAsteroids, 10000); // kreiranje novih asteroida svakih 10 sekundi
        this.movingUp = false;
        this.movingDown = false;
        this.movingRight = false;
        this.movingLeft = false;
    },
    stop : function() {
        clearInterval(this.interval);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y, type) {
    this.type = type;
    this.width = width;
    this.height = height;
    // igrac ima preddefiniranu brzinu, dok se brzina asteroida nasumicno generira
    if (type === 'player') {
        this.speed_x = 10;
        this.speed_y = 10;
    } else {
        this.speed_x = Math.floor(Math.random() * (15 - (-15) + 1)) - 15;
        this.speed_y = Math.floor(Math.random() * (15 - (-15) + 1)) - 15;
    }
    this.x = x;
    this.y = y;
    this.update = function() {
        // updateanje teksta u gornjem lijevom kutu ekrana
        var c = document.getElementById("myGameCanvas");
        var ctx = c.getContext("2d");
        ctx.font = "20px Georgia";
        myGameArea.elapsedTime = new Date() - myGameArea.startTime;
        var minutes = Math.floor(myGameArea.elapsedTime / (1000 * 60));
        var seconds = Math.floor((myGameArea.elapsedTime % (1000 * 60)) / 1000);
        var milliseconds = myGameArea.elapsedTime % 1000;
        var formattedTime = `${minutes}:${seconds}:${milliseconds}`;
        ctx.fillText('Current time: ' + formattedTime, 10, 50);

        minutes = Math.floor(myGameArea.personalBest / (1000 * 60));
        seconds = Math.floor((myGameArea.personalBest % (1000 * 60)) / 1000);
        milliseconds = myGameArea.personalBest % 1000;
        formattedTime = `${minutes}:${seconds}:${milliseconds}`;
        ctx.fillText('Personal best: ' + formattedTime, 10, 70);

        // updateanje komponenti u igri (player i asteroidi)
        ctx = myGameArea.context;
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.shadowBlur = 40;
        ctx.shadowColor = "black";
        ctx.fillStyle = color;
        ctx.fillRect(this.width / -2, this.height / -2, this.width, this.height);
        ctx.restore();
    }
    this.newPos = function() {
        // funkcija koja namjestava novi polozaj igraca i asteroida
        if (this.type === 'player') {
            // ako je igrac dosao do ruba canvasa, premjesti ga na suprotni dio mape
            if (this.y < 0) {
                this.y += myGameArea.canvas.height;
            } else if (this.x < 0) {
                console.log(this.x);
                this.x += myGameArea.canvas.width;
            } else if (this.x > myGameArea.canvas.width) {
                this.x -= myGameArea.canvas.width;
            } else if (this.y > myGameArea.canvas.height) {
                this.y -= myGameArea.canvas.height;
            }

            // kretanje igraca
            if (this.movingUp) {
                this.y -= this.speed_y 
            } else if (this.movingDown) {
                this.y += this.speed_y
            } else if (this.movingRight) {
                this.x += this.speed_x;
            } else if (this.movingLeft) {
                this.x -= this.speed_x;
            }
        } else {
            // ogranicavanje kretanja asteroida, kod iz prezentacije modificiran
            if (this.x - this.width / 2 < myGameArea.min)
            this.speed_x = -this.speed_x;
            else if ((this.x + this.width / 2) >= myGameArea.maxAsteroidWidth)
            this.speed_x = -this.speed_x;
            if (this.y - this.height / 2 < myGameArea.min)
            this.speed_y = -this.speed_y;
            else if ((this.y + this.height / 2) >= myGameArea.maxAsteroidHeight)
            this.speed_y = -this.speed_y;
            this.x += this.speed_x;
            this.y -= this.speed_y;

        }
    }
    this.detectCollision = function() {
        // za svaki asteroid gleda se nalazi li se unutar igraca po dimenzijama
        myGameArea.asteroids.forEach(asteroid => {
            if (this.x < asteroid.x + asteroid.width && this.x + this.width> asteroid.x && this.y < asteroid.y + asteroid.height && this.y + this.height > asteroid.y) {
                // pohranjivanje najboljeg vremena u localStorage koristeci HTML5 Web Storage API
                if (localStorage.getItem("personalBest") < myGameArea.elapsedTime) {
                    localStorage.setItem("personalBest", myGameArea.elapsedTime);
                }
                location.reload();
            }
        })
    }
}

function updateGameArea() {
    // svakih 20 milisekundi poziva se ponovni render scene s novim podacima 
    myGameArea.clear();
    myGamePiece.newPos();
    myGamePiece.update();
    console.log(myGameArea.asteroids.length);
    myGameArea.asteroids.forEach(asteroid => {
        asteroid.newPos();
        asteroid.update();
    });
    myGamePiece.detectCollision();
}

function addNewAsteroids() {
    // generiranje novih 10 asteroida svakih 10 sekundi
    var grd = myGameArea.canvas.getContext("2d").createLinearGradient(0, 0, 170, 0);
    grd.addColorStop(0, "gray");
    grd.addColorStop(1, "white");
    for (var i = 0; i < 10; i++) {
        // generiranje nasumicne visine i sirine polazaja asteroida sve dok se ne nalazi izvan canvasa
        let randomWidth, randomHeight;
        do {
            randomWidth = Math.floor(Math.random() * (window.innerWidth + 500 - (-500) + 1)) - 500;
        } while (randomWidth >= 0 && randomWidth <= window.innerWidth);

        do {
            randomHeight = Math.floor(Math.random() * (window.innerHeight + 500 - (-500) + 1)) - 500;
        } while (randomHeight >= 0 && randomHeight <= window.innerHeight);

        var asteroid = new component(30, 30, grd, randomWidth, randomHeight, 'asteroid');
        myGameArea.asteroids.push(asteroid);
    }
}

// eventlisteneri koji prate u kojem se smjeru krece igrac
document.addEventListener('keydown', function(event) {
    switch(event.key) {
        case 'ArrowUp':
            myGamePiece.movingUp = true;
            break;
        case 'ArrowDown':
            myGamePiece.movingDown = true;
            break;
        case 'ArrowLeft':
            myGamePiece.movingLeft = true;
            break;
        case 'ArrowRight':
            myGamePiece.movingRight = true;
            break;
    }
});

document.addEventListener('keyup', function(event) {
    switch(event.key) {
        case 'ArrowUp':
            myGamePiece.movingUp = false;
            break;
        case 'ArrowDown':
            myGamePiece.movingDown = false;
            break;
        case 'ArrowLeft':
            myGamePiece.movingLeft = false;
            break;
        case 'ArrowRight':
            myGamePiece.movingRight = false;
            break;
    }
});